var express = require('express');
const { rawListeners } = require('../app');
const { addFollow, deleteFollow, tryTestError } = require('../db/users');
const { isValidAddress } = require('../helpers');
var router = express.Router();

/* FOLLOW */

router.get('/', function(req, res, next) {
    res.render('follow', {user: res.user});
});

router.post('/addAddress', async function(req, res, next) {

    const newAddress = req.body.newAddress;

    if (!isValidAddress(newAddress)) {
        res.render('follow', { error_message: "Address '" + newAddress + "' is not valid", user: res.user });
    } else {
        await addFollow(res.user, newAddress, newAddress);
        res.redirect('/follow');
    }
});

router.get('/delete/:address', async function(req, res, next) {

    await deleteFollow(res.user, req.params.address);

    res.redirect('/follow');
});

// testing if app will run again after error
router.get('/tryerror', async function(req, res, next) {

    let w = await tryTestError();

    res.render('follow', {e: w});
});
module.exports = router;
