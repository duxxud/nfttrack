var express = require('express');
const { getNFTTrades } = require('../db/users');
var router = express.Router();

/* FEED */

const cards_per_page = 20;

router.get('/', async function(req, res, next) {

    let nftTrades = await getNFTTrades(res.user, cards_per_page, 0);

    res.render('feed', { user: res.user, nftTrades: nftTrades });
});

router.get('/page/:page', async function(req, res, next) {

    let page = req.params.page;
    let nftTrades = await getNFTTrades(res.user, cards_per_page, page * cards_per_page);

    res.render('feed_cards', { user: res.user, nftTrades: nftTrades });
});

module.exports = router;
