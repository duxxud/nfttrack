const { MongoClient } = require("mongodb");

const uri = process.env.MONGO_URI;

/*
 user {
    address,
    numOfFollowers,
    following,
    lastNFTcheckBlock,
 }

 nftTrade {
    requestedForAddress,
    blockNumber,
    timeStamp,
    txHash,
    txValue,
    tradeFrom,
    tradeTo,
    nftContractAddress, 
    nftName,
    nftSymbol,
    nftTokenId,
    nftTokenMetadata,
 }
*/

async function createUser(users, userAddress) {
  const newUser = {
      address: userAddress,
      numOfFollowers: 0,
      following: [],
      // NFTtrades: [],
      lastNFTcheckBlock: 0,
  }

  // don't check trades for 0x0 address to not make a huge queue
  if (newUser.address == "0x0000000000000000000000000000000000000000") {
      newUser.lastNFTcheckBlock = 999999999;
  }

  await users.insertOne(newUser);

  return newUser;
}

async function getUser(userAddress) {
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const nftTrack = client.db("nfttrack");
    const users = nftTrack.collection("users");

    const query = { address: userAddress };

    let user = await users.findOne(query);

    if (!user) {
       user = await createUser(users, userAddress);
    }

    return user;

  } finally {
    await client.close();
  }
}

async function addFollow(user, newAddressToFollow, nickname) {
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const nftTrack = client.db("nfttrack");
    const users = nftTrack.collection("users");
    
    const filterUser = { address: user.address };
    const updateUser = {
      $addToSet: {
        following:
          {
            address: newAddressToFollow,
            nickname: nickname
          }
      },
    };

    await users.updateOne(filterUser, updateUser);


    let followedUser = await getUser(newAddressToFollow);
    const filterFollowedUser = { address: newAddressToFollow };
    const updateFollowedUser = { 
        $inc: {
          numOfFollowers: 1
        }
    }

    await users.updateOne(filterFollowedUser, updateFollowedUser);

  } finally {
    await client.close();
  }
  
}

async function deleteFollow(user, addressToFollow) {

  const client = new MongoClient(uri);
  try {
    await client.connect();
    const nftTrack = client.db("nfttrack");
    const users = nftTrack.collection("users");
    
    const filterUser = { address: user.address };
    const updateUser = {
      $pull: {
        following:
          {
            address: addressToFollow
          }
      },
    };

    await users.updateOne(filterUser, updateUser);

    const filterFollowedUser = { address: addressToFollow };
    const updateFollowedUser = { 
        $inc: {
          numOfFollowers: -1
        }
    }

    await users.updateOne(filterFollowedUser, updateFollowedUser);


  } finally {
    await client.close();
  }

}

async function getLastUpdatedUser() {
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const nftTrack = client.db("nfttrack");
    const users = nftTrack.collection("users");

    let lastUpdatedUsers = await users.find().sort({lastNFTcheckBlock:1}).limit(1).toArray();

    return lastUpdatedUsers[0];

  } finally {
    await client.close();
  }
}

async function updateTrades(user, newTrades, untilBlockNumber) {
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const nftTrack = client.db("nfttrack");
    const users = nftTrack.collection("users");
    const nftTrades= nftTrack.collection("nfttrades");

    if (newTrades.length > 0) {
      await nftTrades.insertMany(newTrades);
    }

    const filterUser = { address: user.address };
    const updateUser = {
      $set: {
        lastNFTcheckBlock: untilBlockNumber
      }
    }

    await users.updateOne(filterUser, updateUser);

  } finally {
    await client.close();
  }
}

async function getNFTTrades(user, limit, skip) {
  const client = new MongoClient(uri);
  try {
    
    await client.connect();
    const nftTrack = client.db("nfttrack");
    const nftTrades= nftTrack.collection("nfttrades");

    let following = user.following.map(x => x.address);

    const filter = {
      requestedForAddress: {
         $in: following
      }
    }

    let allTrades = await nftTrades.find(filter).sort({timeStamp:-1}).limit(limit).skip(skip).toArray();

    return allTrades;

  } finally {
    await client.close();
  }
}

async function tryTestError() {
   let t = q;

   if (qq.w == "asd") {
      t = w;
   }

   return t;
}

module.exports = { getUser, addFollow, deleteFollow, getLastUpdatedUser, updateTrades, getNFTTrades, tryTestError}