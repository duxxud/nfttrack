var express = require('express');
var router = express.Router();

/* INDEX */


router.get('/', function(req, res, next) {
  // res.render('index', { title: 'Express' });
  var userAddress = req.cookies['userAddress'];
  if (userAddress) {
     res.redirect('feed');
  } else {
     res.redirect('login');   
  }
});

module.exports = router;
