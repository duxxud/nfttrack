const { getUser } = require("../db/users");
const { isValidAddress } = require("../helpers");

module.exports = async (req, res, next) => {

    userAddress = req.cookies['userAddress'];
    if (!isValidAddress(userAddress)) {
        res.redirect('/login');
        return;
    } else {
        res.user = await getUser(userAddress);
    }

    next();
};