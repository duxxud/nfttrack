var pageNum = 1;

$('.load-more').on('click', function(e) {
    e.preventDefault();
    var $self = $(e.currentTarget);

    if($self.hasClass('loading')) {
        return;
    }
    $self.addClass('loading');
    $self.text('Loading');
  
    var request = $.ajax({
        url: "/feed/page/" + pageNum,
        method: "GET",
        dataType: "html"
    });
       
    request.done(function( data ) {
        $newCards = $(data);
        $newCards.insertBefore($self);
        $self.removeClass('loading');
        $self.text('Load more');

        if (data == "") {
            $self.text("That's all");
            $self.removeClass('btn-info');
            $self.addClass('loading');
        }

        pageNum++;
    });
    
  }); 