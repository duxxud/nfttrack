const https = require('https');
const { format } = require('path');
var fs = require('fs');
const { URL } = require('url');
const { getLastUpdatedUser, updateTrades } = require('../db/users');

const Web3 = require("web3")

const web3 = new Web3(new Web3.providers.HttpProvider(process.env.INFURA_URL));
var ERC721Full_abi = JSON.parse(fs.readFileSync('abi/ERC721Full.json')).abi;

function sleep(ms) {
    if (ms > 0) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}

function doRequest(options) {
    return new Promise((resolve, reject) => {
      const req = https.request(options, (res) => {
        res.setEncoding('utf8');
        let responseBody = '';
  
        res.on('data', (chunk) => {
          responseBody += chunk;
        });
  
        res.on('end', () => {
          
          let responseJSON = {};
          try {
              responseJSON = JSON.parse(responseBody)
          } catch(err) {
              console.log("parse error:");
              console.log(err);
              console.log("responseBody:");
              console.log(responseBody);
          }

          resolve(responseJSON);
        });
      });
  
      req.on('error', (err) => {
        console.log("req error:");
        console.log(err);
        resolve({});
      });
  
      req.end();
    });
}

async function getTokenMetadata(tokenURI) {

    let uri;
    try {
        uri = new URL(tokenURI);
    } catch(err) {
        console.log("uri parse error:");
        console.log(err);
        return {};
    }

    console.log("uri: ", uri);

    if (uri.hostname == '') return {};
    if (uri.protocol == "data:") return {};

    var options = {
        host: uri.hostname,
        path: uri.pathname,
        timeout: 1000,
    };

    if (uri.protocol == "ipfs:") {
        options.host = "ipfs.io";
        options.path = "/ipfs" + uri.pathname;
    }

    let nftMetadata = await doRequest(options);

    if (nftMetadata.image != null && nftMetadata.image.startsWith("ipfs://")) {
        nftMetadata.image = nftMetadata.image.replace("ipfs://", "http://ipfs.io/ipfs/");
    }

    return nftMetadata;
}

async function updateNFTs(user, response) { 

    let rawTrades = response.result;

    let userNFTtrades = [];

    for(let i=0; i<rawTrades.length; i++) {

        let trade = rawTrades[i];

        let txData = await web3.eth.getTransaction(trade.hash);
        let txValue = txData["value"];

        const contract = new web3.eth.Contract(ERC721Full_abi, trade.contractAddress);
        
        let tokenURI = "";

        try {
            tokenURI = await contract.methods.tokenURI(trade.tokenID).call();
        } catch(err) {
            console.log("contract token uri error call:")
            console.error(err);
            continue;
        }

        let tokenMetadata = [];
        if (tokenURI != "") {
            console.log("tokenUri: ", tokenURI);
            try {
                tokenMetadata = await getTokenMetadata(tokenURI);
            } catch(err) {
                console.log("token metadata error: ");
                console.log(err);
                continue;
            }
        }
        
        userNFTtrades.push({
            requestedForAddress: user.address,
            blockNumber: trade.blockNumber,
            timeStamp: trade.timeStamp,
            txHash: trade.hash,
            txValue: txValue,
            tradeFrom: trade.from,
            tradeTo: trade.to,
            nftContractAddress: trade.contractAddress,
            nftName: trade.tokenName,
            nftSymbol: trade.tokenSymbol,
            nftTokenId: trade.tokenID,
            nftTokenMetadata: tokenMetadata,
        }); 

        console.log(userNFTtrades[userNFTtrades.length - 1]);

        await sleep(10);
    }

    return userNFTtrades;
}

// getting all new trades for last updated user
async function trackNFT(finishedTrackNFTCallback) {

    let user = await getLastUpdatedUser();

    console.log('cron update for user ', user.address);

    let currentBlock = await web3.eth.getBlockNumber();

    let fromBlock = Math.max(user.lastNFTcheckBlock + 1, currentBlock - 1000000);

    var options = {
        host: process.env.ETHERSCAN_API_URL,
        path: `/api?module=account&action=tokennfttx&address=${user.address}&startblock=${fromBlock}&endblock=${currentBlock}&sort=asc&apikey=${process.env.ETHERSCAN_API_KEY}`,
        timeout: 1000,
    };

    let userTradesWithMetadata = {};
    try {
        let response = await doRequest(options);
        userTradesWithMetadata = await updateNFTs(user, response);
    } catch(err) {
        console.log("nft list response error: ");
        console.log(err);
    }
    
    await updateTrades(user, userTradesWithMetadata, currentBlock);

    finishedTrackNFTCallback();
}

module.exports = { trackNFT };