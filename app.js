var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cron = require('node-cron');
require('dotenv').config();

const { trackNFT } = require('./cron/ERC721TrackEtherscan');

var auth = require('./middleware/auth');

var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var feedRouter = require('./routes/feed');
var followRouter = require('./routes/follow');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/feed', auth, feedRouter);
app.use('/follow', auth, followRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


// setup cron jobs
var cronTrackNFTrunning = false;

function finishedTrackNFTCallback() {
  cronTrackNFTrunning = false;
}

cron.schedule('*/5 * * * * *', async function() {
    if (cronTrackNFTrunning == true) {
       return;
    }

    cronTrackNFTrunning = true;
    await trackNFT(finishedTrackNFTCallback);
});

module.exports = app;
