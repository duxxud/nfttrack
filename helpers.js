const Web3 = require("web3")
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.INFURA_URL));

function isValidAddress(address) {
    return web3.utils.isAddress(address);
}

module.exports = { isValidAddress };