var express = require('express');
const { isValidAddress } = require('../helpers');
var router = express.Router();


/* LOGIN */


router.get('/', function(req, res, next) {
    res.render('login');
});

router.get('/logout', function(req, res, next) {
    res.clearCookie('userAddress');
    res.render('login');
});

router.post('/', async function (req, res, next) {
    const userAddress = req.body.userAddress;

    if (!isValidAddress(userAddress)) {
        res.render('login', {error_message: "Address '" + userAddress +"' is not valid"});
        return;
    } else {

        res.cookie('userAddress', userAddress, { maxAge: 900000 });
        res.redirect('/');
    }
});

module.exports = router;
